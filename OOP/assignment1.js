class Animal {
    constructor(name, hungry) {
        this.name = name;
        this.hungry = hungry;
    }
    doMove() {
        console.log(`${this.name} Ayo Lari..`);
    }
    doEat() {
        console.log(`${this.name}, Silahkan Makan `);
    }  
}