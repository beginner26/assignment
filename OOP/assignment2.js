class Animal {
    constructor(name, hungry) {
        this.name = name;
        this.hungry = hungry;
    }
    doMove() {
        console.log(`${this.name} Ayo Lari..`);
    }
    doEat() {
        console.log(`${this.name}, Silahkan Makan `);
    }  
}
const gajah = new Animal("Jojo", true);
const burung = new Animal("Jhonny", false);
gajah.doEat();
burung.doMove();
burung.doEat();