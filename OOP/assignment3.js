class Animal {
    constructor(name, hungry) {
        this.name = name;
        this.hungry = hungry;
    }
    doEat() {
        console.log("Silahkan Makan");
        this.hungry = false;
    }  
    doMove() {
        console.log("Gerak.. Gerak...");
        this.hungry = true;
    }
}

class Dog extends Animal {
    constructor(name, hungry) {
        super(name, hungry)
    } 

    doHowl() {
        console.log("Anjing Melolong!")
    }
}

class Bird extends Animal {
    constructor(name, hungry) {
        super(name, hungry)
    } 

    doFly() {
        console.log("Burung nya terbang :'( ")
    }
}

const pipit = new Bird("Tweety", false);
const pittbull = new Dog("Brad", false);

pipit.doFly();
pittbull.doHowl();