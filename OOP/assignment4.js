class Animal {
    _namaPemilik = '';
    constructor(name, hungry) {
        this.name = name;
        this.hungry = hungry;
    }
    doEat() {
        console.log("Silahkan Makan");
        this.hungry = false;
    }  
    doMove() {
        console.log("Gerak.. Gerak...");
        this.hungry = true;
    }
    setNamaPemilik(nama) {
        if(nama.trim().length > 0) {
            this._namaPemilik = nama;
        }else{
            this._namaPemilik = "Unknown"
        }
    }
    getNamaPemilik() {
        return this._namaPemilik;
    }
}
const burung = new Animal("Beo", false);
burung.setNamaPemilik('Alex');
console.log(burung.getNamaPemilik());